import discord

from asyncio import sleep, Queue
from gtts import gTTS
import uuid
import os

import logging

read_queues = {}

async def queue_for_reading(origMSG, messages):
    channel_id = origMSG.author.voice.channel.id

    should_start = False

    if channel_id not in read_queues:
        read_queues[channel_id] = Queue()
        should_start = True

    read_queues[channel_id].put_nowait(messages)

    return should_start

async def read_queue(channel):
    q = read_queues[channel.id]
    voiceClient = await channel.connect()
    while not q.empty():
        item = await q.get()
        await readMultiple(voiceClient, item)

    del read_queues[channel.id]
    await voiceClient.disconnect()
    voiceClient.cleanup()

async def readMultiple(voiceClient, messages):
    logging.debug("Initiating voice client")

    for message in messages:
        if isinstance(message, int):
            await sleep(message)
        else:
            await readOut(voiceClient, message)

    logging.debug("Disconnecting voice client")


async def readOut(voiceClient, TTSContents):
    fileName = str(uuid.uuid4()) + ".mp3"

    logging.debug(f'Writting message to file "{fileName}"')

    gTTS(text=TTSContents, lang="da", slow=False).save(fileName)

    logging.debug(f'Reading message from file "{fileName}"')

    voiceClient.play(discord.FFmpegPCMAudio(source=fileName))

    while voiceClient.is_playing():
        await sleep(1)

    logging.debug(f'Removing file with name "{fileName}"')

    os.remove(fileName)
