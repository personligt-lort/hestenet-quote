import requests
from bs4 import BeautifulSoup as bs

from random import randint

import logging


def getPageSoup(url):
    logging.debug(f'Requesting page from "{url}"')
    return bs(requests.get(url).content, "html.parser")


def getRandomPost(postType="OT"):
    overviewURL = f"https://www.heste-nettet.dk/forum/1/{str(randint(10000, 150000))}"

    pageSoup = getPageSoup(overviewURL)

    posts = pageSoup.select("tr[valign=middle]")
    postCount = len(posts)

    while True:
        currentPost = posts[randint(0, postCount - 1)]
        if postType in currentPost.text:
            return f'https://www.heste-nettet.dk{currentPost.select("a[href]")[0].get("href")}'


def extractTable(soup):
    postDetails = {}

    postDetails["title"] = soup.select("span[itemprop=name]")[0].text
    postDetails["author"] = (
        soup.select("td[itemprop=author]")[0].text.split("(")[0].strip()
    )
    postDetails["date"] = soup.select("meta[itemprop=datePublished]")[0].get("content")

    content = soup.select("div.rsPost")[0].text
    postDetails["content"] = (
        (content[:800].strip() + "...") if len(content) > 800 else content.strip()
    )

    return postDetails


async def getPostContents(*, postURL=None, getComments=True):
    if not postURL:
        postURL = getRandomPost()

    postSoup = getPageSoup(postURL)

    messages = postSoup.select("table.margin_for_print")

    post = {"url": postURL, "OP": extractTable(messages.pop(0))}

    if getComments:
        post["comments"] = [extractTable(message) for message in messages]

    return post
